#include <msp430.h>				

#define HL1_PORT_DIR P2DIR
#define HL1_PORT_SEL P2SEL
#define HL1_PORT_OUT P2OUT
#define HL1_BIT      BIT3

#define HL2_PORT_DIR P2DIR
#define HL2_PORT_SEL P2SEL
#define HL2_PORT_OUT P2OUT
#define HL2_BIT      BIT4

#define HL3_PORT_DIR P6DIR
#define HL3_PORT_SEL P6SEL
#define HL3_PORT_OUT P6OUT
#define HL3_BIT      BIT3

#define HL4_PORT_DIR P6DIR
#define HL4_PORT_SEL P6SEL
#define HL4_PORT_OUT P6OUT
#define HL4_BIT      BIT5

typedef enum {
	WHITE_LED,
	RED_LED,
	GREEN_LED,
	BLUE_LED
} LedId;

void setupLeds()
{
	HL1_PORT_SEL &= ~HL1_BIT;
	HL1_PORT_DIR |=  HL1_BIT;
	HL1_PORT_OUT &= ~HL1_BIT;

	HL2_PORT_SEL &= ~HL2_BIT;
	HL2_PORT_DIR |=  HL2_BIT;
	HL2_PORT_OUT &= ~HL2_BIT;

	HL3_PORT_SEL &= ~HL3_BIT;
	HL3_PORT_DIR |=  HL3_BIT;
	HL3_PORT_OUT &= ~HL3_BIT;

	HL4_PORT_SEL &= ~HL4_BIT;
	HL4_PORT_DIR |=  HL4_BIT;
	HL4_PORT_OUT &= ~HL4_BIT;
}

inline static void ToggleLED(LedId ledId)
{
	switch(ledId) {
	case WHITE_LED:
		HL1_PORT_OUT ^= HL1_BIT;
		break;
	case RED_LED:
		HL2_PORT_OUT ^= HL2_BIT;
		break;
	case GREEN_LED:
		HL3_PORT_OUT ^= HL3_BIT;
		break;
	case BLUE_LED:
		HL4_PORT_OUT ^= HL4_BIT;
		break;
	default:
		break;
	}
}

static void SetLED(LedId ledId, int State)
{
	switch(ledId) {
	case WHITE_LED:
		if(State)
			HL1_PORT_OUT |= HL1_BIT;
		else
			HL1_PORT_OUT &= ~HL1_BIT;
		break;
	case RED_LED:
		if(State)
			HL2_PORT_OUT |= HL2_BIT;
		else
			HL2_PORT_OUT &= ~HL2_BIT;
		break;
	case GREEN_LED:
		if(State)
			HL3_PORT_OUT |= HL3_BIT;
		else
			HL3_PORT_OUT &= ~HL3_BIT;
		break;
	case BLUE_LED:
		if(State)
			HL4_PORT_OUT |= HL4_BIT;
		else
			HL4_PORT_OUT &= ~HL4_BIT;
		break;
	default:
		break;
	}
}

// IR_LED_BUTTON
#define SB1_PORT_DIR P2DIR
#define SB1_PORT_SEL P2SEL
#define SB1_PORT_REN P2REN
#define SB1_PORT_IES P2IES
#define SB1_PORT_IFG P2IFG
#define SB1_PORT_IE  P2IE
#define SB1_BIT      BIT0

// WHITE_LED_BUTTON
#define SB3_PORT_DIR P2DIR
#define SB3_PORT_SEL P2SEL
#define SB3_PORT_REN P2REN
#define SB3_PORT_IES P2IES
#define SB3_PORT_IFG P2IFG
#define SB3_PORT_IE  P2IE
#define SB3_BIT      BIT2

#pragma vector=PORT1_VECTOR
__interrupt void PORT1_INTERRUPT(void)
{
	P1IFG = 0;
}

#pragma vector=PORT2_VECTOR
__interrupt void PORT2_INTERRUPT(void)
{
	if (SB1_PORT_IFG & SB1_BIT) {
		SB1_PORT_IFG &= ~SB1_BIT;
		ToggleLED(GREEN_LED);
	}

	if (SB3_PORT_IFG & SB3_BIT) {
		SB3_PORT_IFG &= ~SB3_BIT;
		ToggleLED(BLUE_LED);
	}

	P2IFG = 0;
}

void setupButtons()
{
	SB1_PORT_SEL &= ~SB1_BIT;
	SB1_PORT_DIR &= ~SB1_BIT;
	SB1_PORT_REN |=  SB1_BIT; // with a pulldown resistor
	SB1_PORT_IES |=  SB1_BIT; // ���������� �� �������� �� 1 � 0
	SB1_PORT_IFG &= ~SB1_BIT; // ��� �������������� ������������ ������������ ����������
	SB1_PORT_IE  |=  SB1_BIT; // ��������� ���������� ��� SB1

	SB3_PORT_SEL &= ~SB3_BIT;
	SB3_PORT_DIR &= ~SB3_BIT;
	SB3_PORT_REN |=  SB3_BIT; // with a pulldown resistor
	SB3_PORT_IES |=  SB3_BIT; // ���������� �� �������� �� 1 � 0
	SB3_PORT_IFG &= ~SB3_BIT; // ��� �������������� ������������ ������������ ����������
	SB3_PORT_IE  |=  SB3_BIT; // ��������� ���������� ��� SB3
}

void setup()
{
	setupButtons();
	setupLeds();
	__enable_interrupt();
}

int main(void) {
	WDTCTL = WDTPW | WDTHOLD;	// Stop watchdog timer

	setup();

	for(;;) {
		volatile unsigned int i;	// volatile to prevent optimization

		ToggleLED(WHITE_LED);

		i = 50000;					// SW Delay
		do {
			--i;
		}
		while(i != 0);
	}

	return 0;
}
