#include "driverlib.h"

// === LEDS ===

#define HL1_PORT GPIO_PORT_P2
#define HL1_PIN  GPIO_PIN3

#define HL2_PORT GPIO_PORT_P2
#define HL2_PIN  GPIO_PIN4

#define HL3_PORT GPIO_PORT_P6
#define HL3_PIN  GPIO_PIN3

#define HL4_PORT GPIO_PORT_P6
#define HL4_PIN  GPIO_PIN5

typedef enum {
	WHITE_LED,
	RED_LED,
	GREEN_LED,
	BLUE_LED
} LedId;

void setupLeds()
{
    GPIO_setAsOutputPin(HL1_PORT, HL1_PIN);
    GPIO_setOutputLowOnPin(HL1_PORT, HL1_PIN);
    GPIO_setAsOutputPin(HL2_PORT, HL2_PIN);
    GPIO_setOutputLowOnPin(HL2_PORT, HL2_PIN);
    GPIO_setAsOutputPin(HL3_PORT, HL3_PIN);
    GPIO_setOutputLowOnPin(HL3_PORT, HL3_PIN);
    GPIO_setAsOutputPin(HL4_PORT, HL4_PIN);
    GPIO_setOutputLowOnPin(HL4_PORT, HL4_PIN);
}

inline static void ToggleLED(LedId ledId)
{
	uint8_t selectedPort;
	uint16_t selectedPins;

	switch(ledId) {
	case WHITE_LED:
		selectedPort = HL1_PORT;
		selectedPins = HL1_PIN;
		break;
	case RED_LED:
		selectedPort = HL2_PORT;
		selectedPins = HL2_PIN;
		break;
	case GREEN_LED:
		selectedPort = HL3_PORT;
		selectedPins = HL3_PIN;
		break;
	case BLUE_LED:
		selectedPort = HL4_PORT;
		selectedPins = HL4_PIN;
		break;
	default:
		return;
	}

	GPIO_toggleOutputOnPin(selectedPort, selectedPins);
}

static void SetLED(LedId ledId, int State)
{
	uint8_t selectedPort;
	uint16_t selectedPins;

	switch(ledId) {
	case WHITE_LED:
		selectedPort = HL1_PORT;
		selectedPins = HL1_PIN;
		break;
	case RED_LED:
		selectedPort = HL2_PORT;
		selectedPins = HL2_PIN;
		break;
	case GREEN_LED:
		selectedPort = HL3_PORT;
		selectedPins = HL3_PIN;
		break;
	case BLUE_LED:
		selectedPort = HL4_PORT;
		selectedPins = HL4_PIN;
		break;
	default:
		return;
	}

	if(State) {
		GPIO_setOutputHighOnPin(selectedPort, selectedPins);
	}
	else {
		GPIO_setOutputLowOnPin(selectedPort, selectedPins);
	}
}

// === BUTTONS ===

// IR_LED_BUTTON
#define SB1_PORT GPIO_PORT_P2
#define SB1_PIN  GPIO_PIN0

// WHITE_LED_BUTTON
#define SB3_PORT GPIO_PORT_P2
#define SB3_PIN  GPIO_PIN2

#pragma vector=PORT1_VECTOR
__interrupt void PORT1_INTERRUPT(void)
{
	P1IFG = 0;
}

#pragma vector=PORT2_VECTOR
__interrupt void PORT2_INTERRUPT(void)
{
	// TODO: �������� �������� �� �������, ��������� ����� �� ����������, ���� � ����������� ������ ����� 50 �����������.

	if (GPIO_getInterruptStatus(SB1_PORT, SB1_PIN)) {
		GPIO_clearInterruptFlag(SB1_PORT, SB1_PIN);
		ToggleLED(GREEN_LED);
	}

	if (GPIO_getInterruptStatus(SB3_PORT, SB3_PIN)) {
		GPIO_clearInterruptFlag(SB3_PORT, SB3_PIN);
		ToggleLED(BLUE_LED);
	}

	P2IFG = 0;
}

void setupButtons()
{
	GPIO_setAsInputPinWithPullUpResistor(SB1_PORT, SB1_PIN);
	GPIO_interruptEdgeSelect(SB1_PORT, SB1_PIN, GPIO_HIGH_TO_LOW_TRANSITION);
	GPIO_clearInterruptFlag(SB1_PORT, SB1_PIN);
	GPIO_enableInterrupt(SB1_PORT, SB1_PIN);

	GPIO_setAsInputPinWithPullUpResistor(SB3_PORT, SB3_PIN);
	GPIO_interruptEdgeSelect(SB3_PORT, SB3_PIN, GPIO_HIGH_TO_LOW_TRANSITION);
	GPIO_clearInterruptFlag(SB3_PORT, SB3_PIN);
	GPIO_enableInterrupt(SB3_PORT, SB3_PIN);
}

void setup()
{
    WDT_A_hold(WDT_A_BASE);

	setupButtons();
	setupLeds();

	__enable_interrupt();
}

int main(void)
{
	setup();

	for(;;) {
		volatile unsigned int i;	// volatile to prevent optimization

		ToggleLED(RED_LED);

		i = 50000;					// SW Delay
		do {
			--i;
		}
		while(i != 0);
	}

	return (0);
}
