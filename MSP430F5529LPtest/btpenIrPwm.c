#include <driverlib.h>
#include "btpenIrPwm.h"
#include "btpenLeds.h"

#if 1

uint16_t period    = 1000;
uint16_t dutyCycle = 100;

void setupIrPwm(void)
{
    GPIO_setAsOutputPin(IR_LED_EN_PORT, IR_LED_EN_PIN);
    GPIO_setOutputLowOnPin(IR_LED_EN_PORT, IR_LED_EN_PIN);

    GPIO_setAsPeripheralModuleFunctionOutputPin(IR_LED_DIM_PORT, IR_LED_DIM_PIN);
    GPIO_setOutputLowOnPin(IR_LED_DIM_PORT, IR_LED_DIM_PIN);

    TIMER_A_generatePWM(
        TIMER_A0_BASE,                 		// Set up Timer A0
//        TIMER_A_CLOCKSOURCE_ACLK,           // Use ACLK (slower clock)
        TIMER_A_CLOCKSOURCE_SMCLK,
//        TIMER_A_CLOCKSOURCE_DIVIDER_1,      // Input clock = ACLK / 1 = 32KHz
        TIMER_A_CLOCKSOURCE_DIVIDER_4,      // Input clock = ACLK / 1 = 32KHz
        period,								// Period
        TIMER_A_CAPTURECOMPARE_REGISTER_3,  // Use CCR2 for compare
        TIMER_A_OUTPUTMODE_RESET_SET,       // Toggle provides a good waveform to watch with LED
        dutyCycle							// Duty cycle (20/40 = 1/2 duty cycle)
    );

//    TIMER_A_startCounter(TIMER_A0_BASE,
//        TIMER_A_UP_MODE
//    );
}

#endif

#if 0

uint16_t period    = 32000;
uint16_t dutyCycle = 16000;

volatile unsigned int tickCount = 0;

#pragma vector = TIMER0_A0_VECTOR
__interrupt void CCR0_ISR_INTERRUPT(void)
{
    ++tickCount;
}

#pragma vector = TIMER0_A1_VECTOR
__interrupt void CCRn_ISR_INTERRUPT(void)
{
    switch(__even_in_range(TA0IV, 10)) {
    case 0x00:	// none
        break;
    case 0x02:	// CCR1 IFG
        break;
    case 0x04:	// CCR2 IFG
        break;
    case 0x06:	// CCR3 IFG
        GPIO_setOutputLowOnPin(HL4_PORT, HL4_PIN);
        GPIO_setOutputLowOnPin(IR_LED_DIM_PORT, IR_LED_DIM_PIN);
        break;
    case 0x08:	// CCR4 IFG
        break;
    case 0x0A:	// CCR5 IFG
        break;
    case 0x0C:	// CCR6 IFG
        break;
    case 0x0E:	// TA0IFG
        GPIO_setOutputHighOnPin(HL4_PORT, HL4_PIN);
        GPIO_setOutputHighOnPin(IR_LED_DIM_PORT, IR_LED_DIM_PIN);
        break;
    default: _never_executed();
    }
}

void setupIrPwm(void)
{
    GPIO_setAsOutputPin(IR_LED_EN_PORT, IR_LED_EN_PIN);
    GPIO_setOutputLowOnPin(IR_LED_EN_PORT, IR_LED_EN_PIN);

//	GPIO_setAsOutputPin(IR_LED_DIM_PORT, IR_LED_DIM_PIN);
    GPIO_setAsPeripheralModuleFunctionOutputPin(IR_LED_DIM_PORT, IR_LED_DIM_PIN);
    GPIO_setOutputLowOnPin(IR_LED_DIM_PORT, IR_LED_DIM_PIN);

    TIMER_A_configureUpMode(
        TIMER_A0_BASE,
        TIMER_A_CLOCKSOURCE_ACLK,
//		TIMER_A_CLOCKSOURCE_SMCLK,
        TIMER_A_CLOCKSOURCE_DIVIDER_1,
        period,
        TIMER_A_TAIE_INTERRUPT_ENABLE,
        TIMER_A_CCIE_CCR0_INTERRUPT_ENABLE,
//		TIMER_A_CCIE_CCR0_INTERRUPT_DISABLE,
        TIMER_A_DO_CLEAR
    );

    TIMER_A_initCompare(
        TIMER_A0_BASE,
        TIMER_A_CAPTURECOMPARE_REGISTER_3,
        TIMER_A_CAPTURECOMPARE_INTERRUPT_ENABLE,
//		TIMER_A_OUTPUTMODE_OUTBITVALUE,
        TIMER_A_OUTPUTMODE_RESET_SET,
        dutyCycle
    );

    TIMER_A_clearTimerInterruptFlag(TIMER_A0_BASE);
    TIMER_A_clearCaptureCompareInterruptFlag(
        TIMER_A0_BASE,
        TIMER_A_CAPTURECOMPARE_REGISTER_0 + TIMER_A_CAPTURECOMPARE_REGISTER_3
    );
    TIMER_A_startCounter(
        TIMER_A0_BASE,
        TIMER_A_UP_MODE
    );
}

#endif

void irLedEnable(int state)
{
    if (state) {
        TIMER_A_startCounter(TIMER_A0_BASE,
            TIMER_A_UP_MODE
        );
        GPIO_setOutputHighOnPin(IR_LED_EN_PORT, IR_LED_EN_PIN);
    }
    else {
        GPIO_setOutputLowOnPin(IR_LED_EN_PORT, IR_LED_EN_PIN);
        GPIO_setOutputLowOnPin(IR_LED_DIM_PORT, IR_LED_DIM_PIN);
        TIMER_A_stop(TIMER_A0_BASE);
    }
}
