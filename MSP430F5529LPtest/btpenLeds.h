#ifndef BTPEN_LEDS_H
#define BTPEN_LEDS_H

#define HL1_PORT GPIO_PORT_P2
#define HL1_PIN  GPIO_PIN3

#if defined (__MSP430F5528__)
#   define HL2_PORT GPIO_PORT_P2
#   define HL2_PIN  GPIO_PIN4
#elif defined (__MSP430F5529__)
#   define HL2_PORT GPIO_PORT_P1
#   define HL2_PIN  GPIO_PIN0
#else
#   error "unsupported"
#endif

#if defined (__MSP430F5528__)
#   define HL3_PORT GPIO_PORT_P6
#   define HL3_PIN  GPIO_PIN3
#elif defined (__MSP430F5529__)
#   define HL3_PORT GPIO_PORT_P4
#   define HL3_PIN  GPIO_PIN7
#else
#   error "unsupported"
#endif

#define HL4_PORT GPIO_PORT_P6
#define HL4_PIN  GPIO_PIN5

typedef enum {
    WHITE_LED,
    RED_LED,
    GREEN_LED,
    BLUE_LED
} LedId;

void setupLeds(void);
void toggleLED(LedId ledId);
void setLED(LedId ledId, int state);

#endif /* BTPEN_LEDS_H */
