#ifndef BTPEN_BUTTONS_H
#define BTPEN_BUTTONS_H

#include "types.h"

// IR_LED_BUTTON
#if defined (__MSP430F5528__)
#   define SB1_PORT GPIO_PORT_P2
#   define SB1_PIN  GPIO_PIN0
#elif defined (__MSP430F5529__)
#   define SB1_PORT GPIO_PORT_P2
#   define SB1_PIN  GPIO_PIN1
#else
#   error "unsupported"
#endif

// WHITE_LED_BUTTON
#if defined (__MSP430F5528__)
#   define SB3_PORT GPIO_PORT_P2
#   define SB3_PIN  GPIO_PIN2
#elif defined (__MSP430F5529__)
#   define SB3_PORT GPIO_PORT_P1
#   define SB3_PIN  GPIO_PIN1
#else
#   error "unsupported"
#endif

// POWER_BUTTON
#if defined (__MSP430F5528__)
#   define SB2_PORT GPIO_PORT_P2
#   define SB2_PIN  GPIO_PIN7
#elif defined (__MSP430F5529__)
#   define SB2_PORT GPIO_PORT_P2
#   define SB2_PIN  GPIO_PIN7
#else
#   error "unsupported"
#endif

typedef enum {
    IR_LED_BUTTON    = 0,
    SB1_BUTTON       = 0,
    S1_BUTTON        = 0,

    POWER_BUTTON     = 1,
    SB2_BUTTON       = 1,

    WHITE_LED_BUTTON = 2,
    SB3_BUTTON       = 2,
    S2_BUTTON        = 2,
} ButtonId;

void setupButtons(void);
BOOL isButtonPressed(ButtonId buttonId);

#endif /* BTPEN_BUTTONS_H */
