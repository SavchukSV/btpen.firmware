#ifndef BTPEN_IR_PWM_H
#define BTPEN_IR_PWM_H

#define IR_LED_EN_PORT GPIO_PORT_P1
#define IR_LED_EN_PIN  GPIO_PIN3

#define IR_LED_DIM_PORT GPIO_PORT_P1
#define IR_LED_DIM_PIN  GPIO_PIN4

void setupIrPwm(void);
void irLedEnable(int state);

#endif /* BTPEN_IR_PWM_H */

