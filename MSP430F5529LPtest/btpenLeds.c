#include <driverlib.h>
#include "btpenLeds.h"

void setupLeds()
{
    GPIO_setAsOutputPin(HL1_PORT, HL1_PIN);		// WHITE LED
    GPIO_setOutputLowOnPin(HL1_PORT, HL1_PIN);

    GPIO_setAsOutputPin(HL2_PORT, HL2_PIN);		// RED LED
    GPIO_setOutputLowOnPin(HL2_PORT, HL2_PIN);

    GPIO_setAsOutputPin(HL3_PORT, HL3_PIN);		// GREEN LED
    GPIO_setOutputLowOnPin(HL3_PORT, HL3_PIN);

    GPIO_setAsOutputPin(HL4_PORT, HL4_PIN);		// BLUE_LED
    GPIO_setOutputLowOnPin(HL4_PORT, HL4_PIN);
}

void toggleLED(LedId ledId)
{
    uint8_t selectedPort;
    uint16_t selectedPins;

    switch(ledId) {
    case WHITE_LED:
        selectedPort = HL1_PORT;
        selectedPins = HL1_PIN;
        break;
    case RED_LED:
        selectedPort = HL2_PORT;
        selectedPins = HL2_PIN;
        break;
    case GREEN_LED:
        selectedPort = HL3_PORT;
        selectedPins = HL3_PIN;
        break;
    case BLUE_LED:
        selectedPort = HL4_PORT;
        selectedPins = HL4_PIN;
        break;
    default:
        return;
    }

    GPIO_toggleOutputOnPin(selectedPort, selectedPins);
}

void setLED(LedId ledId, int state)
{
    uint8_t selectedPort;
    uint16_t selectedPins;

    switch(ledId) {
    case WHITE_LED:
        selectedPort = HL1_PORT;
        selectedPins = HL1_PIN;
        break;
    case RED_LED:
        selectedPort = HL2_PORT;
        selectedPins = HL2_PIN;
        break;
    case GREEN_LED:
        selectedPort = HL3_PORT;
        selectedPins = HL3_PIN;
        break;
    case BLUE_LED:
        selectedPort = HL4_PORT;
        selectedPins = HL4_PIN;
        break;
    default:
        return;
    }

    if(state) {
        GPIO_setOutputHighOnPin(selectedPort, selectedPins);
    }
    else {
        GPIO_setOutputLowOnPin(selectedPort, selectedPins);
    }
}
