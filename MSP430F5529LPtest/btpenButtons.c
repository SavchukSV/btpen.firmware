#include <driverlib.h>
#include "btpenButtons.h"
#include "btpenLeds.h"
#include "btpenIrPwm.h"

static volatile BOOL buttonIsPressed[3] = {FALSE, FALSE, FALSE};

#pragma vector=PORT1_VECTOR
#pragma vector=PORT2_VECTOR
__interrupt void PORT2_INTERRUPT(void)
{
    volatile unsigned int i;
    if (GPIO_getInterruptStatus(SB1_PORT, SB1_PIN)) {
        GPIO_disableInterrupt(SB1_PORT, SB1_PIN);
        GPIO_clearInterruptFlag(SB1_PORT, SB1_PIN);
        buttonIsPressed[SB1_BUTTON] = true;
    }

    if (GPIO_getInterruptStatus(SB3_PORT, SB3_PIN)) {
        GPIO_disableInterrupt(SB3_PORT, SB3_PIN);
        GPIO_clearInterruptFlag(SB3_PORT, SB3_PIN);
        buttonIsPressed[SB3_BUTTON] = true;
    }

    P2IFG = 0;

//  switch(__even_in_range(P2IV, 10)) {
//  case 0x00:  // none
//      break;
//  case 0x02:  // pin 0
//      break;
//  case 0x04:  // pin 1
//      break;
//  case 0x06:  // pin 2
//      break;
//  case 0x08:  // pin 3
//      break;
//  case 0x0A:  // pin 4
//      break;
//  case 0x0C:  // pin 5
//      break;
//  case 0x0E:  // pin 6
//      break;
//  case 0x10:  // pin 7
//      break;
//  default: _never_executed();
//  }
}

void setupButtons(void)
{
    GPIO_setAsInputPinWithPullUpResistor(SB1_PORT, SB1_PIN);
    GPIO_interruptEdgeSelect(SB1_PORT, SB1_PIN, GPIO_HIGH_TO_LOW_TRANSITION);
    GPIO_clearInterruptFlag(SB1_PORT, SB1_PIN);
    GPIO_enableInterrupt(SB1_PORT, SB1_PIN);

    GPIO_setAsInputPinWithPullUpResistor(SB3_PORT, SB3_PIN);
    GPIO_interruptEdgeSelect(SB3_PORT, SB3_PIN, GPIO_HIGH_TO_LOW_TRANSITION);
    GPIO_clearInterruptFlag(SB3_PORT, SB3_PIN);
    GPIO_enableInterrupt(SB3_PORT, SB3_PIN);
}

BOOL isButtonPressed(ButtonId buttonId)
{
    int rc = FALSE;
    if (buttonIsPressed[buttonId] == TRUE) {
        rc = TRUE;
    }
    switch(buttonId) { // check if button still pressed
    case SB1_BUTTON:
        if (GPIO_getInputPinValue(SB1_PORT, SB1_PIN) == 0) {
            rc = TRUE;
        }
        GPIO_enableInterrupt(SB1_PORT, SB1_PIN);
        break;
    case SB2_BUTTON:
        break;
    case SB3_BUTTON:
        if (GPIO_getInputPinValue(SB3_PORT, SB3_PIN) == 0) {
            rc = TRUE;
        }
        GPIO_enableInterrupt(SB3_PORT, SB3_PIN);
        break;
    default:
        break;
    }
    buttonIsPressed[buttonId] = FALSE;

    return rc;
}
