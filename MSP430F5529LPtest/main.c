#include <stdio.h>
#include <driverlib.h>
#include "btpenClocks.h"
#include "btpenButtons.h"
#include "btpenLeds.h"
#include "btpenIrPwm.h"

#define ONE_SECOND	8192000

void setup(void)
{
    WDT_A_hold(WDT_A_BASE);

    setupClocks();
    setupButtons();
    setupLeds();
    setupIrPwm();

    __enable_interrupt();
}

void loop(void) {
    if (isButtonPressed(SB1_BUTTON)) {
        irLedEnable(0);
        setLED(GREEN_LED, 1);
        if (TA0CCR3 < 990) {
            TA0CCR3 += 10;
        }
        printf("1:%d/1000\n", TA0CCR3);
        irLedEnable(1);
    }
    if (isButtonPressed(SB2_BUTTON)) {
        printf("2:%d/1000\n", TA0CCR3);
    }
    if (isButtonPressed(SB3_BUTTON)) {
        irLedEnable(0);
        setLED(GREEN_LED, 0);
        if (TA0CCR3 > 10) {
            TA0CCR3 -= 10;
        }
        printf("3:%d/1000\n", TA0CCR3);
        irLedEnable(1);
    }
}

void main(void)
{
    volatile int i;

    setup();

    while(1) {
        for(i = 0; i < 5; ++i) {
            __delay_cycles(ONE_SECOND / 5);
            loop();
        }
        toggleLED(RED_LED);
    }
}
